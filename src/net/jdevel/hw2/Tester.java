package net.jdevel.hw2;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class Tester {
    private Task task;
    private String path;

    public Tester(Task task, String path) {
        this.task = task;
        this.path = path;
    }

    public void runTests() {
        int nr = 0;
        while (true) {
            File inFile = new File(String.format("%s/test.%d.in", path, nr));
            File outFile = new File(String.format("%s/test.%d.out", path, nr));
            if (!inFile.exists() || !outFile.exists()) {
                break;
            }
            boolean result = RunTest(inFile, outFile);
            System.out.println("Test #" + nr + "-" + result + "\n");
            nr++;
        }
    }

    private boolean RunTest(File inFile, File outFile) {
        try {
            List<String> data = Files.readAllLines(inFile.toPath(), Charset.defaultCharset());
            String expect = Files.readString(outFile.toPath()).trim();
            long startTime = System.currentTimeMillis();
            String actual = task.run(data);
            long endTime = System.currentTimeMillis();
            System.out.println("Execution time = " + (endTime - startTime));
            return actual.equals(expect);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
