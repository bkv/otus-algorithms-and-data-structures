package net.jdevel.hw2;

import java.util.List;

public interface Task {
    String run(List<String> data);
}
