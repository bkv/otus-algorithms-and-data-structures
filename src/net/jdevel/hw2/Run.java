package net.jdevel.hw2;

import net.jdevel.hw2.tasks.LuckyTicketTask;
import net.jdevel.hw2.tasks.StringLengthTask;

public class Run {
    public static void main(String[] args) {
        Task stringLengthTask = new StringLengthTask();
        Tester stringLengthTester = new Tester(stringLengthTask, "data/0.String");
        stringLengthTester.runTests();

        Task luckyTicketTask = new LuckyTicketTask();
        Tester luckyTicketTester = new Tester(luckyTicketTask, "data/1.Tickets");
        luckyTicketTester.runTests();


    }
}
