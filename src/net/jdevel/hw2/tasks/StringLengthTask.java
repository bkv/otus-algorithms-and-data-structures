package net.jdevel.hw2.tasks;

import net.jdevel.hw2.Task;

import java.util.List;

public class StringLengthTask implements Task {
    @Override
    public String run(List<String> data) {
        return String.valueOf(data.get(0).length());
    }
}
