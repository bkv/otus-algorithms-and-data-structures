package net.jdevel.hw2.tasks;

import net.jdevel.hw2.Task;

import java.util.List;

public class LuckyTicketTask implements Task {
    int n;
    int q;
    @Override
    public String run(List<String> data) {
        n = Integer.parseInt(data.get(0));
        q = 0;
        nextDigit(0,0,0);
        return String.valueOf(q);
    }

    private void nextDigit(int digit, int sum1, int sum2)  {
        if(digit == n) {
            if(sum1 == sum2)
                q++;
            return;
        }
        for(int a = 0; a <= 9; a++)
            for (int b = 0; b <= 9; b++)
                nextDigit(digit + 1, sum1 + a, sum2 + b);
    }
}
