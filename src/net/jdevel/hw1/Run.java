package net.jdevel.hw1;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class Run {

    private static BiPredicate<Integer, Integer> spell01 = (x, y) -> (x < y);
    private static BiPredicate<Integer, Integer> spell02 = (x, y) -> (x == y);
    private static BiPredicate<Integer, Integer> spell03 = (x, y) -> (y == 24 - x);
    private static BiPredicate<Integer, Integer> spell04 = (x, y) -> (x + y < 30);
    private static BiPredicate<Integer, Integer> spell08 = (x, y) -> (x * y == 0);
    private static BiPredicate<Integer, Integer> spell06 = (x, y) -> (x < 10 || y < 10);
    private static BiPredicate<Integer, Integer> spell07 = (x, y) -> (!(x < 16 || y < 16)) ;
    private static BiPredicate<Integer, Integer> spell19 = (x, y) -> (x * y == 0 || x == 24 || y == 24);

    public static void main(String[] args) {
        cast(spell01);
        cast(spell02);
        cast(spell03);
        cast(spell04);
        cast(spell08);
        cast(spell19);
        cast(spell06);
        cast(spell07);

    }

    private static void cast(BiPredicate<Integer, Integer> spell) {
        for (int x = 0; x < 25; x++) {
            for (int y = 0; y < 25; y++) {
                System.out.print(spell.test(x, y) ? "# " : ". ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
